tgNamespace.tgContainer.registerInnerGate("performance", function (oWrapper,oLocalConfData)
{
    this.oWrapper = oWrapper;
    this.sLocalConfData = oLocalConfData;

    this._construct = function(){
        if(this.oWrapper.debug("performance","_construct"))debugger;

        this.oWrapper.registerInnerGateFunctions("performance", "pageload", this.trackPerformance);
    }


    this.trackPerformance = function(oArgs){
        if (this.oWrapper.debug("performance", "trackPerformance")) debugger;


        // sessionCookie
        if(this.oWrapper.readCookie(oLocalConfData["sPerfCookie"]) === null)
        {
            var sUniqueId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
            this.oWrapper.setCookie(oLocalConfData["sPerfCookie"], sUniqueId, null);
        }

        sSessId = this.oWrapper.readCookie(oLocalConfData["sPerfCookie"]);

        //get screen Info
        var sScreenRes = typeof window.screen.availWidth !== "undefined" && typeof window.screen.availHeight !== "undefined" ? window.screen.availWidth + "x" + window.screen.availHeight : "notAvailable";
        var sUsedScreenRes = typeof window.screen.width !== "undefined" && typeof window.screen.height !== "undefined" ? window.screen.width + "x" + window.screen.height : "notAvailable";
        var sPixDef = typeof window.screen.pixelDepth !== "undefined" ? window.screen.pixelDepth : "notAvailable";

        //get hostname and url
        var sHostname = typeof document.location.hostname !== "undefined" ? document.location.hostname : "notAvailable";
        var sUrl = typeof window.location.href !== "undefined" ? window.location.href.split('?')[0] : "notAvailable";
        

        //get browser
        var browser = (function(){
            var ua= navigator.userAgent, tem,
                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        })();

        //get browser_family
        var browser_family = browser.match(/^\D+/)[0].slice(0, -1);


        //Daten erstellen
        var sData = {
            "sessionId" : sSessId,
            "dimensions" : {
                "browser_family" : browser_family,
                "browser" : browser,
                "available_resolution" : sScreenRes,
                "color_depth" : sPixDef,
                "flash_loaded" : bHasFlash,
                "used_resolution" : sUsedScreenRes,
                "hostname" : sHostname,
                "url" : sUrl
            },
            "timing" : window.performance["timing"]

        };


        //senden
        var url = oLocalConfData["SERVER-URL"];
        var track = new Image();
        track.src = url + "?base64=true&json=" + btoa(JSON.stringify(sData));
    }
});




tgNamespace.tgContainer.registerInnerConfig("performance",
//LIVE CONF
    {
        countConfigs: 1,
        countFuncLibs: 0,
        "SERVER-URL" : "https://hervis.perfmon.funkenbox.net/track",
        "sPerfCookie" : "W18-performanceID"

    },
//TEST CONF
    {
        "SERVER-URL" : "https://hervis.perfmon.funkenbox.net/test/track" //TEST-ID
    });
